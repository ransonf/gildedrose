PROJET GILTED ROSE
==================

Contexte
--------

Ce projet a été réalisé par Clément Lavallee et Florian Ranson dans le cadre du cours d'architecture applicative.

Avancement
----------

Nous avons réussi en cours à avancer jusqu'à la partie sur l'achat, la vente d'item et la balance. 

Nous avons pu terminer cette partie chez nous, mais les deux autres parties sur la limitation de vente et d'achat de certains articles ainsi que la relic a été plus compliqué. En effet, malgré plusieurs échanges avec d'autres élèves, nous n'avons pas réussi à répondre entièrement à ces demandes. 

Nous nous sommes donc reconcentrer sur le code que nous avions écrit afin de l'otpimiser, le rendre plus clair et compréhensible.

Conclusion
----------

Ce projet a été très enrichissant pour nous, au dela de faire du code qui fonctionne, nous avons pu comprendre la pertinence d'être rigoureux dans sa qualité de code. Afin premièrement de faciliter les futurs évolutions et développements, mais aussi pour aider les autres développeurs. Etant en alternance en dev, c'est quelque chose que nous pourrons directement mettre en plus dans notre méthode de travail.
