package test;

import item.*;
import console.InventoryRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryRepository implements InventoryRepository {

    private List< Item > items;

    public InMemoryRepository() {
        this.items = new ArrayList<>();
        items.add( new GenericItem( "Generic Item", 3, 3 ) );
        items.add( new GenericItem( "Generic Item", -1, 4 ) );
        items.add( new GenericItem( "Generic Item", 3, 0 ) );
        items.add( new AgedItem( "Aged Brie", 4, 5 ) );
        items.add( new AgedItem( "Aged Brie", 4, 50 ) );
        items.add( new LegendaryItem( "Sulfuras", 4, 80 ) );
        items.add( new EventItem( "Backstage Pass", 15, 4 ) );
        items.add( new EventItem( "Backstage Pass", 10, 4 ) );
        items.add( new EventItem( "Backstage Pass", 5, 4 ) );
        items.add( new EventItem( "Backstage Pass", -1, 4 ) );
        items.add( new ConjuredItem( "Conjured", 3, 6) );
        items.add( new ConjuredItem( "Conjured", -1, 10) );

    }

    @Override
    public List<Item> getInventory() {
        return this.items;
    }

    @Override
    public void saveInventory(List<Item> items) {
        this.items = items;
    }
}
