package test;

import boss.BossInteractor;
import inventory.InventoryInteractor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import item.AgedItem;
import item.Item;
import org.junit.Before;
import org.junit.Test;
import shop.ShopInteractor;

public class GildedRoseTest {

    private InventoryInteractor inventoryInteractorMemory;
    private ShopInteractor shopInteractor;
    private BossInteractor bossInteractor;
    private InMemoryRepository memoryRepository;
    private InMemoryBalance memoryBalance;

    @Before
    public void setup() throws FileNotFoundException, IOException{
        this.memoryRepository = new InMemoryRepository();
        this.memoryBalance = new InMemoryBalance();
        this.inventoryInteractorMemory = new InventoryInteractor( memoryRepository );
        this.shopInteractor = new ShopInteractor( memoryRepository, memoryBalance );
        this.bossInteractor = new BossInteractor( memoryBalance );
    }
    

    @Test
    public void Should_HaveSellInOnItems() {
        assertEquals(3, inventoryInteractorMemory.getInventory().get(0).getSellIn());
    }

    @Test
    public void Should_HaveQualityOnItems() {
    	assertEquals(3, inventoryInteractorMemory.getInventory().get(0).getQuality());
    }

   @Test 
   public void Should_DecreaseQualityAndSellInEveryDay()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(2, inventoryInteractorMemory.getInventory().get(0).getSellIn());
        assertEquals(2, inventoryInteractorMemory.getInventory().get(0).getQuality());
    }
   
   
   @Test 
   public void Should_DecreaseQualityTwiceAsFastAfterSellIn()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(2, inventoryInteractorMemory.getInventory().get(1).getQuality());
    }
   
   @Test 
   public void Should_NeverHaveNegativeQuality()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(0, inventoryInteractorMemory.getInventory().get(2).getQuality());
    }
   
   @Test
   public void Should_IncreaseAgedBrie()
   {
       inventoryInteractorMemory.updateInventory();
       assertEquals(6, inventoryInteractorMemory.getInventory().get(3).getQuality());
   }
   
   @Test 
   public void Should_NeverHaveQualityOverFifty()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(50, inventoryInteractorMemory.getInventory().get(4).getQuality());
    }
   
   @Test 
   public void Should_NeverUpdateSulfuras()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(4, inventoryInteractorMemory.getInventory().get(5).getSellIn());
        assertEquals(80, inventoryInteractorMemory.getInventory().get(5).getQuality());
    }
   
   @Test 
   public void Should_IncreaseBackStagePassQuality()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(5, inventoryInteractorMemory.getInventory().get(6).getQuality());
        assertEquals(6, inventoryInteractorMemory.getInventory().get(7).getQuality());
        assertEquals(7, inventoryInteractorMemory.getInventory().get(8).getQuality());
        assertEquals(0, inventoryInteractorMemory.getInventory().get(9).getQuality());
    }
   
   @Test 
   public void Should_DecreaseQualityTwiceForConjured()
    {
        inventoryInteractorMemory.updateInventory();
        assertEquals(4, inventoryInteractorMemory.getInventory().get(10).getQuality());
        assertEquals(6, inventoryInteractorMemory.getInventory().get(11).getQuality());
    }

    @Test
    public void inventoryCountTest(){
        Map< String, Integer > map = inventoryInteractorMemory.getInventoryCount();
        assertEquals( 3, map.get( "Generic Item" ) );
        assertEquals( 2, map.get( "Aged Brie" ) );
        assertEquals( 1, map.get( "Sulfuras" ) );
        assertEquals( 4, map.get( "Backstage Pass" ) );
        assertEquals( 2, map.get( "Conjured" ) );
    }

    @Test
    public void buyItemFail(){
        Item item = inventoryInteractorMemory.getInventory().get( 0 );
        memoryBalance.saveBalance( 0 );
        shopInteractor.buyItem( item );
        assertEquals( 0, memoryBalance.getBalance() );
    }

    @Test
    public void buyItemSuccess(){
        Item item = inventoryInteractorMemory.getInventory().get( 0 );
        Integer expected = memoryBalance.getBalance() - item.getValue();
        shopInteractor.buyItem( item );
        assertEquals( expected, memoryBalance.getBalance() );
    }

    @Test
    public void sellItemFail(){
        Item item = inventoryInteractorMemory.getInventory().get( 0 );
        Integer expected = memoryBalance.getBalance();

        List< Item > items = new ArrayList<>();
        items.add( new AgedItem( "Aged Brie", 4, 5 ) );
        memoryRepository.saveInventory( items );

        shopInteractor.sellItem( item );
        assertEquals( expected, memoryBalance.getBalance() );
    }

    @Test
    public void sellItemSuccess(){
        Item item = inventoryInteractorMemory.getInventory().get( 0 );
        Integer expected = memoryBalance.getBalance() + item.getValue();
        shopInteractor.sellItem( item );
        assertEquals( expected, memoryBalance.getBalance() );
    }

    @Test
    public void getBalanceTest(){
        Integer expected = memoryBalance.getBalance();
        assertEquals( expected, bossInteractor.getBalance() );
    }

}
