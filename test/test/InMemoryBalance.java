package test;

import console.BalanceRepository;

public class InMemoryBalance implements BalanceRepository {

    private Integer balance;

    public InMemoryBalance(){
        this.balance = 20;
    }

    @Override
    public Integer getBalance() {
        return balance;
    }

    @Override
    public void saveBalance(Integer balance) {
        this.balance = balance;
    }
}
