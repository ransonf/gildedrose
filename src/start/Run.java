package start;
import java.io.FileNotFoundException;
import java.io.IOException;

import console.ConsoleApplication;

public class Run {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        ConsoleApplication consoleApplication = new ConsoleApplication();
        consoleApplication.DisplayInventoryCommand();
    }
}
