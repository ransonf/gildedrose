package boss;

import console.BalanceRepository;

public class BossInteractor implements BossAction{

    private BalanceRepository balanceRepository;

    public BossInteractor( BalanceRepository balanceRepository ) {
        this.balanceRepository = balanceRepository;
    }

    @Override
    public Integer getBalance() {
        return balanceRepository.getBalance();
    }
}
