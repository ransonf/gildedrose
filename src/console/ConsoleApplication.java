package console;

import java.util.Map;
import java.util.Scanner;

import inventory.InventoryInteractor;
import inventory.InventoryUpdater;
import inventory.InventoryViewer;
import item.Item;

public class ConsoleApplication {

	private InventoryUpdater inventoryUpdater;
	private InventoryViewer inventoryViewer;
	private InventoryRepository inventorRepository;
	private Scanner scanner;
	private Map<String,Integer> inventoryCount;

	public ConsoleApplication(){
		InventoryInteractor interactor = new InventoryInteractor( inventorRepository );
		inventoryViewer = interactor;
		inventoryUpdater = interactor;
		scanner = new Scanner(System.in);
	}

	public void DisplayInventoryCommand(){
		System.out.println("Liste des items :\n");
		for(Item item : inventoryViewer.getInventory()) {
			System.out.println("* "+item.getName()+"\n\tSellIn : "+item.getSellIn()+"\n\tQuality : "+item.getQuality()+"\n\n");
		}
		nextCommand();
		inventoryUpdater = new InventoryInteractor( inventorRepository );
		inventoryViewer = new InventoryInteractor( inventorRepository );
	}

	public void DisplayInventoryByCountCommand(){
		System.out.println("Liste des items regroupés :\n");
		inventoryCount=inventoryViewer.getInventoryCount();
		inventoryCount.forEach((key, value) -> System.out.println(key + " : " + value));
		nextCommand();
	}

	public void UpdateInventoryCommand(){
		inventoryUpdater.updateInventory();
		DisplayInventoryCommand();
		nextCommand();
	}

	public void nextCommand() {
		System.out.println("\nQue souhaitez-vous faire ? Commandes dispo : \"update\" \"count\"");
		System.out.println("\b") ;
		String souhait = scanner.nextLine();

		switch(souhait) {
			case "update":
				UpdateInventoryCommand();
				break;

			case "count":
				DisplayInventoryByCountCommand();
				break;
		}
	}

}
