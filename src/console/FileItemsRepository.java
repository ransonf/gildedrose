package console;

import item.AgedItem;
import item.ConjuredItem;
import item.EventItem;
import item.GenericItem;
import item.Item;
import item.LegendaryItem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileItemsRepository implements InventoryRepository {

	private List< Item > items;
	private static final String PATHFILE = "rsc/items.csv";

	public FileItemsRepository() throws FileNotFoundException, IOException {
		
		items= new ArrayList<>();
		
		List<List<String>> records = new ArrayList<>();
		try ( BufferedReader br = new BufferedReader( new FileReader( PATHFILE ) ) ) {
		    String line;
		    while( ( line = br.readLine() ) != null ) {
		        String[] values = line.split( ";" );
		        records.add( Arrays.asList( values ) );
		    }
		}
		
		for(int i=1;i<records.size();i++) {

			switch ( records.get(i).get(0) ){
				case "\"GenericItem\"":
					items.add(new GenericItem(records.get(i).get(1),Integer.parseInt(records.get(i).get(2)),Integer.parseInt(records.get(i).get(3))));
					break;

				case "\"AgedItem\"":
					items.add(new AgedItem(records.get(i).get(1),Integer.parseInt(records.get(i).get(2)),Integer.parseInt(records.get(i).get(3))));
					break;

				case "\"LegendaryItem\"":
					items.add(new LegendaryItem(records.get(i).get(1),Integer.parseInt(records.get(i).get(2)),Integer.parseInt(records.get(i).get(3))));
					break;

				case "\"EventItem\"":
					items.add(new EventItem(records.get(i).get(1),Integer.parseInt(records.get(i).get(2)),Integer.parseInt(records.get(i).get(3))));
					break;

				case "\"ConjuredItem\"":
					items.add(new ConjuredItem(records.get(i).get(1),Integer.parseInt(records.get(i).get(2)),Integer.parseInt(records.get(i).get(3))));
					break;
			}
			
		}
		
		for(Item item : items) {
			item.setName(item.getName().replace("\"", ""));
		}
	}
	
    @Override
    public List<Item> getInventory() {
        return this.items;
    }

    @Override
    public void saveInventory(List<Item> items) {
    	this.items=items;
    }
}
