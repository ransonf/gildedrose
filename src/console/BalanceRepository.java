package console;

public interface BalanceRepository {

    Integer getBalance();

    void saveBalance( Integer balance );
}
