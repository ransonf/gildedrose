package console;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileBalanceRepository implements BalanceRepository{
	
	private int balance;	
	private static final String PATHFILE = "rsc/balance.txt";
	
	public FileBalanceRepository() throws FileNotFoundException, IOException {
		

		try ( BufferedReader br = new BufferedReader( new FileReader( PATHFILE ) ) ) {
		    String line;
		    while( ( line = br.readLine() ) != null ) {	        
		        this.balance=Integer.parseInt(line);
		    }
		}
	}
    @Override
    public Integer getBalance() {
        return this.balance;
    }

    @Override
    public void saveBalance( Integer prmBalance ) {
    	this.balance=prmBalance;
    }
}
