package shop;

import item.Item;

public interface ShopAction {

    void sellItem( Item item );

    void buyItem( Item item );
}
