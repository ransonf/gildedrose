 package shop;

import console.BalanceRepository;
import item.Item;
import console.InventoryRepository;

public class ShopInteractor implements ShopAction {

    private InventoryRepository inventoryRepository;
    private BalanceRepository balanceRepository;
    private ItemAcceptor itemAcceptor;

    public ShopInteractor( InventoryRepository inventorRepository, BalanceRepository balanceRepository, ItemAcceptor itemAcceptor ) {
        this.inventoryRepository = inventorRepository;
        this.balanceRepository = balanceRepository;
        this.itemAcceptor = itemAcceptor;
    }

    public ShopInteractor( InventoryRepository inventorRepository, BalanceRepository balanceRepository ) {
        this.inventoryRepository = inventorRepository;
        this.balanceRepository = balanceRepository;
        this.itemAcceptor = null;
    }

    @Override
    public void sellItem( Item item ){
        if( itemAcceptor == null || itemAcceptor.acceptItem( item ) ){
            if( inventoryRepository.getInventory().contains( item ) ){
                balanceRepository.saveBalance( balanceRepository.getBalance() + item.getValue() );
                inventoryRepository.getInventory().remove( item );
            }

            this.inventoryRepository.saveInventory( inventoryRepository.getInventory() );
        }
    }

    @Override
    public void buyItem( Item item ){
        if( itemAcceptor == null || itemAcceptor.acceptItem( item ) ) {
            if (balanceRepository.getBalance() >= item.getValue()) {
                balanceRepository.saveBalance(balanceRepository.getBalance() - item.getValue());
                inventoryRepository.getInventory().add(item);
            }

            this.inventoryRepository.saveInventory(inventoryRepository.getInventory());
        }
    }
}
