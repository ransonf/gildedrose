package shop;

import item.Item;

public class ShopJustLegendary implements ItemAcceptor{

    @Override
    public boolean acceptItem(Item item) {
        return item.getClass().getSimpleName() == "LegendaryItem";
    }
}
