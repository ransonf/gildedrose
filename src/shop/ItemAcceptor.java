package shop;

import item.Item;

public interface ItemAcceptor {

    boolean acceptItem( Item item );
}
