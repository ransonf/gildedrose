package inventory;

import item.Item;
import console.InventoryRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryInteractor implements InventoryUpdater, InventoryViewer{

    private InventoryRepository inventoryRepository;

    public InventoryInteractor(InventoryRepository inventorRepository ) {
        this.inventoryRepository = inventorRepository;
    }

    @Override
    public void updateInventory(){
        for( Item item : this.inventoryRepository.getInventory() ){
            item.update();
        }

        this.inventoryRepository.saveInventory( inventoryRepository.getInventory() );
    }

    @Override
    public List<Item> getInventory() {
		return inventoryRepository.getInventory();
	}

    @Override
    public Map< String, Integer> getInventoryCount() {
        Map< String, Integer > map = new HashMap<>();
        for( Item i : inventoryRepository.getInventory() ){
            if( !map.containsKey( i.getName() ) ){
                map.put( i.getName(), 1 );
            }
            else{
                map.replace( i.getName(), map.get( i.getName() ) + 1 );
            }
        }

        return map;
    }

}
