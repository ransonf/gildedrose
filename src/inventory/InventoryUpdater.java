package inventory;

public interface InventoryUpdater{

    void updateInventory();

}
