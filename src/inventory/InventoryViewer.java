package inventory;

import item.Item;

import java.util.List;
import java.util.Map;

public interface InventoryViewer {

    List<Item> getInventory();

    Map<String, Integer> getInventoryCount();
}
