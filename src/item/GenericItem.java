package item;

public class GenericItem extends Item {

    public GenericItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
        this.value = 1;
    }

    @Override
    public void update() {
        this.sellIn--;
        this.quality--;

        if( this.getSellIn() < 0 ){
            this.quality--;
        }

        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }
}
