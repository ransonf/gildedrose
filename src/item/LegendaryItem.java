package item;

public class LegendaryItem extends Item {

    public LegendaryItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
        this.value = 10;
    }

    @Override
    public void update() {
    }
}
