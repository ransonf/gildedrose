package item;

public class EventItem extends Item {
    public EventItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
        this.value = 3;
    }

    @Override
    public void update() {
        if( sellIn > 10 ){
            quality++;
            sellIn--;
        }
        if( sellIn <= 10 && sellIn > 5){
            quality += 2 ;
            sellIn--;
        }
        if( sellIn <= 5){
            quality += 3;
            sellIn--;
        }
        if( sellIn <= 0) {
        	quality=0;
        	sellIn--;
        }

        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }
}
