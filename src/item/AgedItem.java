package item;

public class AgedItem extends Item {

    public AgedItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
        this.value = 5;
    }

    @Override
    public void update() {
        quality++;
        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }
}
