package item;

public class ConjuredItem extends Item {
    public ConjuredItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
        this.value = 2;
    }

    @Override
    public void update() {
        this.sellIn--;
        this.quality = this.quality - 2;

        if( this.getSellIn() < 0 ){
            quality -= 2;
        }

        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }
}
