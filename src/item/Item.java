package item;

public abstract class Item {

    public static final int MAX_QUALITY = 50;
    public static final int MIN_QUALITY = 0;

    protected String name;
    protected int sellIn;
    protected int quality;
    protected int value;

    public abstract void update();

    public String getName() {
        return name;
    }
    
    public void setName(String prmName) {
        this.name=prmName;
    }

    public int getSellIn() {
        return sellIn;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getValue() {
        return value;
    }

    public Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    protected void ceilQualityToFifty(){
        if( quality > MAX_QUALITY ){
            setQuality( MAX_QUALITY );
        }
    }

    protected void floorQualityToZero(){
        if( quality < MIN_QUALITY ){
            setQuality( MIN_QUALITY );
        }
    }
}
